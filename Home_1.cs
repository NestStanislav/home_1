﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите стороны A, B, C.");

            string sideA = Console.ReadLine();
            int a = Convert.ToInt32(sideA);

            string sideB = Console.ReadLine();
            int b = Convert.ToInt32(sideB);

            string sideC = Console.ReadLine();
            int c = Convert.ToInt32(sideC);


            if (c > a || c > b)
            {
                Console.WriteLine("В прямоугольнике нельзя разместить ни одного квадрата");
            }
            else
            {
                int length1 = a / c;
                int length2 = b / c;

                int numberSquare = length1 * length2;
                Console.WriteLine("Можно разместить {0} квадратов", numberSquare);

                int otherArea = a * b - ((int)Math.Pow(c, 2) * numberSquare);
                Console.WriteLine("Оставшаяся площадь {0}", otherArea);
            }
        }       
    }
}
