﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите числа N (N>0)");
            string _n = Console.ReadLine();
            int n = Convert.ToInt32(_n);

            Console.WriteLine("Число было: {0}", n);
            int x = 0;

            while (n > 0)
            {
                x = (x * 10) + (n % 10);
                n /= 10;
            }

            Console.WriteLine("Число стало: {0}", x);
        }
    }
}
